/*
Copyright@ reserved by Luyu Wang
This code is intended to show the principle of numerical PDEs
For private purpose 
*/

#include <iostream>
#include <iomanip>

#include "ClassFunc.h"
using namespace std;



/*-------------------------------- Function 1 ------------------------------------*/
// A- before adding boundary conditions 

double** ClassFunc::FuncA(int N, double h, double* p, double* q, double* f) {

	// creating an arrary to store the matrix A
	double** A = new double* [N + 1];
	for (int i = 0; i < N + 1; i++) {
		A[i] = new double[N + 1];
	}

	// Initialization of the matrix A
	for (int i = 0; i < N + 1; i++) {
		for (int j = 0; j < N + 1; j++) {
			A[i][j] = 0;
		}
	}

	// filling in the elements of A 
	for (int i = 1; i < N; i++) {       // non-boundary nodes

		double a = p[i - 1] / h;
		double b = (p[i - 1] + p[i]) / h + h * q[i];
		double c = p[i] / h;
		double g = h * f[i];

		A[i][i - 1] += -a;
		A[i][i] += b;
		A[i][i + 1] += -c;
	}


	///*
	cout << "--------------------- Matrix A ----------------------" << endl;
	cout << setw(10);
	for (int i = 0; i < N + 1; i++) {
		for (int j = 0; j < N + 1; j++) {
			cout << A[i][j] << setw(10);
			if (j == N) {
				cout << endl;
			}
		}
	}
	//*/


	return A;
}








/*-------------------------------- Function 2 ------------------------------------*/
// RHS- before adding boundary conditions 

double* ClassFunc::FuncRHS(int N, double h, double* f) {

	// creat an array and inilizating it 
	double* RHS = new double[N+1];
	for (int i = 0; i < N + 1; i++) {
		RHS[i] = 0.0;
	}


	// filling in the elements of RHS 
	for (int i = 1; i < N; i++) {       // non-boundary nodes
		double g = h * f[i];
		RHS[i] = g;
	}


	///*
	cout << "------------- RHS ------------" << endl;
	for (int i = 0; i < N + 1; i++) {
		cout << RHS[i] << endl;
	}
	//*/

	return RHS;

}







/*-------------------------------- Function 3 ------------------------------------*/
// A_1- adding boundary conditions 
double** ClassFunc::FuncA_1(double** A, int N, double h, double* p, double* q, double* f, double alpha_0, double alpha_1, double beta_0, double beta_1) {

	// creating an arrary A 
	double** A_1 = new double* [N + 1];
	for (int i = 0; i < N + 1; i++) {
		A_1[i] = new double[N + 1];
	}

	// inilizating matrix A
	for (int i = 0; i < N + 1; i++) {
		for (int j = 0; j < N + 1; j++) {
			A_1[i][j] = 0.0;
		}
	}

	// for the left boundary
	A_1[0][0] = p[0] / h + alpha_0 + h / 2 * q[0];
	A_1[0][1] = -p[0] / h;

	// for the right boundary
	A_1[N][N - 1] = -p[N-1]/h;
	A_1[N][N] = p[N - 1] / h + beta_0 + h / 2 * q[N];


	// filling the other elelments in A_1 by the former A
	for (int i = 1; i < N; i++) {
		for (int j = 0; j < N + 1; j++) {
			A_1[i][j] = A[i][j];
		}
	}



	///*
	cout << "--------------------- Matrix A_1 ----------------------" << endl;
	cout << setw(10);
	for (int i = 0; i < N + 1; i++) {
		for (int j = 0; j < N + 1; j++) {
			cout << A_1[i][j] << setw(10);
			if (j == N) {
				cout << endl;
			}
		}
	}
	//*/


	return A_1;

}










/*-------------------------------- Function 4 ------------------------------------*/
// RHS_1- adding boundary conditions 
double* ClassFunc::RHS_1(double* RHS, int N, double h, double alpha_1, double beta_1, double* f){


	// creating and inilitzing an arrary to store RHS_1
	double* RHS_1 = new double[N+1];
	for (int i = 0; i < N + 1; i++) {
		RHS_1[i] = 0.0;
	}

	RHS_1[0] = alpha_1 + h / 2 * f[0];
	RHS_1[N] = beta_1 + h / 2 * f[N];

	for (int i = 1; i < N; i++) {
		RHS_1[i] = RHS[i];
	}





	///*
	cout << "------------- RHS_1 ------------" << endl;
	for (int i = 0; i < N + 1; i++) {
		cout << RHS_1[i] << endl;
	}
	//*/



	return RHS_1;

}






