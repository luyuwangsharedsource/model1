/*
Copyright@ reserved by Luyu Wang
This code is intended to show the principle of numerical PDEs
For private purpose 
*/

#include <iostream>
#include <time.h>
#include "ClassInput.h"
using namespace std;



/*-------------------------------- Function 1 ------------------------------------*/

double* ClassInput::Funcp(int N, double Valp, int flag_p){

	if (flag_p == 1) {
		double* p = new double[N];
		for (int i = 0; i < N; i++) {
			srand(i);
			double Range = 1 + rand() % 10;  // a + rand() % n, where a- start point, n is the range
			double Valp_new = Range * Valp / 10;
			p[i] = Valp_new;
			//cout << "p: " << p[i] << endl;
		}
		return p;
	}


	if (flag_p == 0) {
		double* p = new double[N];
		for (int i = 0; i < N; i++) {
			p[i] = Valp;
			//cout << "p: " << p[i] << endl;
		}
		return p;
	}
}





/*-------------------------------- Function 2 ------------------------------------*/

double* ClassInput::Funcq(int N, double Valq, int flag_q) {

	if (flag_q == 1) {
		double* q = new double[N+1];
		for (int i = 0; i < N+1; i++) {
			srand(i);
			double Range = rand();
			double Valq_new = Range * Valq;
			q[i] = Valq_new;
			//cout << "q: " << q[i] << endl;
		}
		return q;
	}


	if (flag_q == 0) {
		double* q = new double[N+1];
		for (int i = 0; i < N + 1; i++) {
			q[i] = Valq;
			//cout << "q: " << q[i] << endl;
		}
		return q;
	}
}






/*-------------------------------- Function 3 ------------------------------------*/

double* ClassInput::Funcf(int N, double Valf, int flag_f) {

	if (flag_f == 1) {
		double* f = new double[N+1];
		for (int i = 0; i < N + 1; i++) {
			srand(i);
			double Range = rand();
			double Valf_new = Range * Valf;
			f[i] = Valf_new;
			//cout << "f: " << f[i] << endl;
		}
		return f;
	}


	if (flag_f == 0) {
		double* f = new double[N+1];
		for (int i = 0; i < N + 1; i++) {
			f[i] = Valf;
			//cout << "f: " << f[i] << endl;
		}
		return f;
	}
}




