#include <iostream> 
#include <iomanip>
#include <fstream> 
#include <ctime>
#include <cmath>
#include <vector>


#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <unsupported/Eigen/IterativeSolvers>


#include "Solver_SparseMatrix.h"

using namespace std;
using namespace Eigen;

double* Solver_Sparse(int NumTotalCell, double** PMatrixA, double* PRHS)
{

	int N = NumTotalCell;

	typedef SparseMatrix<double> MyMatrix;  // or SparseMatrix<double> A(N,N)
	MyMatrix A(N,N);
	VectorXd b(N), x(N);


	// ................. Start For elimanate the very small number 
	//for (int i = 0; i < N; i++) {
	//	for (int j = 0; j < N; j++) {
	//		if (abs(PMatrixA[i][j]) <= 1e-5) {
	//			PMatrixA[i][j] = 0.0;
	//		}
	//	}
	//}

	//for (int j = 0; j < N; j++) {
	//	if (abs(PRHS[j]) <= 1e-5) {
	//		PRHS[j] = 0.0;
	//	}
	//}
	// ........................... End For delete the very small number 


	// ................. Start For complement non-zeros entries in diagonal 


	//for (int i = 0; i < N; i++) {
	//	if (abs(PMatrixA[i][i]) == 0) {
	//		PMatrixA[i][i] = 0.1;
	//	}
	//}

	// ........................... End For complement thr non-zeros entries in diagonal 







	//////////////////////////// Normalization of Matrix Cul and b /////////////////////// Nov 19 2020

	///*
	double* vec = new double [N];
	for (int i = 0; i < N; i++) {
		vec[i] = NULL;
	}


	double Basei;

	for (int i = 0; i < N; i++) {
		Basei = 0.0;
		for (int j = 0; j < N; j++) {
			
			vec[j] = PMatrixA[i][j];
			Basei = vec[j] * vec[j] + Basei;
			
			if (j == (N-1)) {	
				Basei = sqrt(Basei);
				PRHS[i] = PRHS[i] / Basei;

				for (int k = 0; k < N; k++) {
					PMatrixA[i][k] = PMatrixA[i][k] / Basei;					
				}
			}
		}
	}
	//*/


	/*
	cout << endl << endl << "------------------" ;
	for (int i = 0; i < N; i++) {
		cout << setprecision(20) << PRHS[i] << endl ;
	}
	cout << endl << endl << "------------------";
	*/


	//////////////////////////// Constructing the algebraic system /////////////////////

	for (int i = 0; i <= (N - 1); i++) {
		for (int j = 0; j <= (N - 1); j++) {
			if ((*(*(PMatrixA + i) + j)) != 0.0) {
				A.insert(i, j) = *(*(PMatrixA + i) + j);
			}
		}
	}


	for (int i = 0; i <= (N - 1); i++) {
		b(i) = *(PRHS + i);
	}

	A.makeCompressed();






	///////////////////////////////////////////////// Solver //////////////////////////////////////////

	int SolverCG        =   0   ;
	int SolverLSCG      =   0   ;
	int SolverBiCGSTAB  =   0   ;
	int	SolverGMRES     =   0   ;


	int	SolverLU        =   1   ;
	int SolverQR        =   0   ;
	int SolverLDLT      =   0   ;




	/* ............................. Iterative methods .............................. */

	// Conjugate gradient
	if (SolverCG == 1) {
		ConjugateGradient<SparseMatrix<double>, Lower | Upper> cg;
		cg.preconditioner();
		cg.compute(A);
		x = cg.solve(b);

		std::cout << "#iterations:     " << cg.iterations() << std::endl;
		std::cout << "estimated error: " << cg.error() << std::endl;
	}



	// LeastSquaresConjugateGradient ------ CG for rectangular least-square problem
	if (SolverLSCG == 1) {

		LeastSquaresConjugateGradient<SparseMatrix<double> > lscg;
		lscg.preconditioner();
		lscg.compute(A);
		x = lscg.solve(b);

		std::cout << "#iterations:     " << lscg.iterations() << std::endl;
		std::cout << "estimated error: " << lscg.error() << std::endl;
	}



	// BiCGSTAB  Iterative stabilized bi-conjugate gradient 
	if (SolverBiCGSTAB == 1) {

		BiCGSTAB<SparseMatrix<double> > bicg;
		bicg.preconditioner();
		bicg.compute(A);
		x = bicg.solve(b);

		std::cout << "#iterations:     " << bicg.iterations() << std::endl;
		std::cout << "estimated error: " << bicg.error() << std::endl;

		/* ... update b ... */
		x = bicg.solve(b); // solve again


	}



	// GMRES 
	if (SolverGMRES == 1) {

		GMRES<SparseMatrix<double>, IdentityPreconditioner> gmres;
		gmres.preconditioner();
		gmres.compute(A);
		x = gmres.solve(b);

		std::cout << "#iterations:     " << gmres.iterations() << std::endl;
		std::cout << "estimated error: " << gmres.error() << std::endl;
	}




	/* ............................. Direct methods .............................. */
	// LU decomposition 
	if (SolverLU == 1) {

		Eigen::SparseLU<Eigen::SparseMatrix<double> > solverA;
		A.makeCompressed();
		solverA.analyzePattern(A);
		solverA.factorize(A);
		if (solverA.info() != Eigen::Success) {
			std::cout << "Oh: Very bad" << "\n";
		}
		else {
			std::cout << "okay computed" << "\n";
		}
		x = solverA.solve(b);

	}

	// QR decomposition
	if (SolverQR == 1) {
		SparseQR< SparseMatrix<double>, COLAMDOrdering<int>> solver;
		solver.compute(A);
		x = solver.solve(b);
	}



	// LDLT
	if (SolverLDLT == 1) {
		SimplicialLDLT< SparseMatrix<double>> solver;
		solver.compute(A);
		x = solver.solve(b);
	}

	




	/////////////////////////////////////////////// Return /////////////////////////////////////////////

	double* p = new double[N];
	for (int i = 0; i <= (N - 1); i++) {
		p[i] = x(i);
	}


	// Results
	//for (int i = 0; i < N;i++) {
	//	if (abs(p[i]) <= 1e-50) {      // appro
	//		p[i] = 0.0;
	//	}
	//}




	/*cout << "Matrix A ============================" << endl;
	cout << setw(5);
	for (int i = 0; i <= (N - 1); i++) {
		for (int j = 0; j <= (N - 1); j++) {
			cout << A(i, j) << ", " << setw(5);
			if (j == (N - 1))
				cout << endl;
		}
	}

	cout << "RHS ============================" << endl;
	for (int i = 0; i <= (N - 1); i++) {
		cout << b(i) << endl;
	}


	cout << "p============================" << endl;
	for (int i = 0; i <= (N - 1); i++) {
		cout << p[i] << endl;
	}*/


	return p;

	delete[] p;
	p = NULL;
}



