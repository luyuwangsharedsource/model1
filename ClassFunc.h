/*
Copyright@ reserved by Luyu Wang
This code is intended to show the principle of numerical PDEs
For private purpose 
*/

#pragma once

#include <iostream>

class ClassFunc {

public:

	// before adding boundary conditions 
	double** FuncA(int N, double h, double* p, double* q, double* f);
	double* FuncRHS(int N, double h, double* f);

	// adding boundary conditions 
	double** FuncA_1(double** A, int N, double h, double* p, double* q, double* f, double alpha_0, double alpha_1, double beta_0, double beta_1);
	double* RHS_1(double* RHS, int N, double h, double alpha_1, double beta_1, double* f);


};

