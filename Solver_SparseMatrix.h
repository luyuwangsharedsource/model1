#ifndef SOLVER_SPARSEMATRIX_H_INCLUDED
#define SOLVER_SPARSEMATRIX_H_INCLUDED


double* Solver_Sparse(int NumTotalCell, double** PMatrixA, double* PRHS);


#endif // SOLVER_SPARSEMATRIX_H_INCLUDED
