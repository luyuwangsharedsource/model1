/*
Copyright@ reserved by Luyu Wang
This code is intended to show the principle of numerical PDEs
For private purpose 
*/

#pragma once

#include <iostream>

class ClassInput {

public:
	/*----------------------------- Grid -----------------------------*/
	int N = 5;               // amount of cells; amount of nodes is N+1
	double L = 1;            // length of the domain (line in 1D case)
	double h = L / N;        // size of each grid

	/*------------------------- Boundary Conditions ---------------------*/
	double alpha_0 = 1;
	double beta_0 = 1;
	double alpha_1 = 1;
	double beta_1 = 10;

	/*--------------------- Definition of Functions ---------------------*/
	double* p;        // the concept of p is based on each cell
	double* q;        // the concept of q and f is basd on each node
	double* f;
	double Valp = 1;  // if p q f are uniform distributed on the domain
	double Valq = 1;  // these values can be adjusted in the related subroutines aftewards
	double Valf = 1;
	int flag_p = 0;   // the flag means 0- uniform, 1- random
	int flag_q = 0;
	int flag_f = 0;

	/*--------------------- Definition of Functions ---------------------*/
	double* Funcp(int N, double Valp, int flag_p);
	double* Funcq(int N, double Valq, int flag_q);
	double* Funcf(int N, double Valf, int flag_f);


};

