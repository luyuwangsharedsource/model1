/*
Copyright@ reserved by Luyu Wang
This code is intended to show the principle of numerical PDEs
For private purpose 
*/

///////////////////////////////////// C++ header files /////////////////////////////
#include <iostream>

////////////////////////////////// Eigen header files /////////////////////////////
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <unsupported/Eigen/IterativeSolvers>

/////////////////////////////////// User header files //////////////////////////////
#include "ClassInput.h"
#include "ClassFunc.h"
#include "Solver_SparseMatrix.h"


//////////////////////////////////// Namespcae /////////////////////////////////////
using namespace std;


//////////////////////////////////// Main function start ///////////////////////////
int main() {

	cout << "Copyright@ by Luyu Wang. " << endl <<endl;

	/*----------------------- Get the input information ------------------*/
	ClassInput Object_Input;

	/* Grid */
	int N = Object_Input.N;
	double L = Object_Input.L;
	double h = Object_Input.h;

	/* Boundary Conditions */
	double alpha_0 = Object_Input.alpha_0;
	double beta_0 = Object_Input.beta_0;
	double alpha_1 = Object_Input.alpha_1;
	double beta_1 = Object_Input.beta_1;

	/* Definition of Functions */
	double* p = Object_Input.p;
	double* q = Object_Input.q;
	double* f = Object_Input.f;
	double Valp = Object_Input.Valp;
	double Valq = Object_Input.Valq;
	double Valf = Object_Input.Valf;
	int flag_p = Object_Input.flag_p;
	int flag_q = Object_Input.flag_q;
	int flag_f = Object_Input.flag_f;
	p = Object_Input.Funcp(N, Valp, flag_p);
	q = Object_Input.Funcq(N, Valq, flag_q);
	f = Object_Input.Funcf(N, Valf, flag_f);


	/*----------------------- Discretized form on internal nodes ------------------*/
	// creat coefficient matrix A and RHS 
	// both of them are of before adding boundary conditions)
	ClassFunc Object_Func;
	double** A = Object_Func.FuncA( N,  h,  p,  q,  f);
	double* RHS = Object_Func.FuncRHS(N, h, f);

	/*----------------------- Treatment of boundary condition ------------------*/
	// adding boundary conditions to matrix A and RHS
	double** A_1 = Object_Func.FuncA_1(A, N, h, p, q, f, alpha_0, alpha_1, beta_0, beta_1);
	double* RHS_1 = Object_Func.RHS_1(RHS, N, h, alpha_1, beta_1, f);

	/*-------------------------------------- Solving ---------------------------------*/
	double* x = Solver_Sparse(N + 1 , A_1, RHS_1);
	

	///*
	cout << "------------- x ------------" << endl;
	for (int i = 0; i < N + 1; i++) {
		cout << x[i] << endl;
	}
	//*/




	/*----------------------- End ------------------*/

	cout << "Programm completed. " << endl;

	cin.get();
	return 0;

}

